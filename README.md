# Ubuntu/Debian package Restful API

This projects exposes details about installed packages on an Ubuntu/Debian system via a JSON Rest API using Flask.

## Requirements
- Python 3.9+
- Flask
- pytest 
- Docker (otpional, for containerized deployment)

## Installation
1. Clone the repository
    ```bash
    git clone https://github.com/swatarianess/flaskProjectStatusChecker.git
    cd flaskProjectStatusChecker
    ```
2. Create and activate a virtual environment
    ```bash
    python3 -m venv .venv
    source .venv/bin/activate
    ```

3. Install the dependencies
    ```bash
    pip install -r requirements.txt
    ```

## Running the Application
1. Set environment variables (optional):
    ```bash
    export FLASK_CONFIG=DevelopmentConfig
    ```
2. Run the Flask application
    ```bash
    flask run
    ```
   The application will be accessible at `http://127.0.0.1:5000`

## Running with Docker
1. Build the Docker image
    ```bash
    docker build -t flask-app .
    ```
2. Run the Docker container:
    ```bash
    docker run -d -p 5000:5000 flask-app
    ```

## Testing
1. Install testing dependencies:
    ```bash
    pip install -r requirements.dev.txt
    ```

2. Run the tests:
    ```bash
    pytest
    ```


## Configuration
Configuration settintgs are handled in `app/config.py`. you can create different configurations for development, testing, and production.

## API Endpoints
List of available API endpoints
- **`GET api/packages`** Retrieves a list of packages
- **`GET api/packages/<package_name>`** Retrieve details of a specific package, including dependencies and reverse dependencies.

## Frontend Routes
* GET /: Lists all the packages
* GET /packages/<package_name>: Lists details of supplied package name

## Improvements

* More Tests
 * Integration tests
 * More robust tests (More edge-cases)
* Frontend with ability to navigate through packages