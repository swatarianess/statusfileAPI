FROM python:3.9-alpine3.20

# Ensure we are in the app dir
WORKDIR /app

# Copy over the requirements
COPY requirements.txt requirements.txt

# Install packages
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the app into the container
COPY . .

ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

EXPOSE 5000

CMD ["flask", "run"]
