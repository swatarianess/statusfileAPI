import pytest
from app import create_app


@pytest.fixture
def packages():
    return {
        'tcpd': {
            'Package': 'tcpd',
            'Description': r"Wietse Venema's TCP wrapper utilities\n Wietse Venema's network logger, also known as TCPD or LOG_TCP.",
            'Depends': 'libc6 (>= 2.4), libwrap0 (>= 7.6-4~)',
            'Version': '7.6.q-21'
        },
        'libwrap0': {
            'Package': 'libwrap0',
            'Description': 'Some description for libwrap0',
            'Version': '7.6-4~'
        },
        'python-pkg-resources': {
            'Package': 'python-pkg-resources',
            'Description': 'Package Discovery and Resource Access using pkg_resources\n The pkg_resources module provides an API for Python libraries to access their resource files.',
            'Depends': 'python (>= 2.6), python (<< 2.8)',
            'Version': '0.6.24-1ubuntu1'
        }
    }


@pytest.fixture
def app():
    app = create_app()
    app.config.update({
        "TESTING": True,
    })
    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def mock_parse_status_file(packages, mocker):
    mocker.patch('app.services.parse_status_file', return_value=packages)
