import pytest

from app.services import get_all_packages, parse_dependencies, get_reverse_dependencies


def test_get_all_packages(client, mock_parse_status_file):
    """
    Tests the normal case for getting all packages
    """
    response = client.get('api/packages')
    expected = [
        {'name': 'tcpd', 'version': '7.6.q-21'},
        {'name': 'libwrap0', 'version': '7.6-4~'},
        {'name': 'python-pkg-resources', 'version': '0.6.24-1ubuntu1'},
    ]

    actual = get_all_packages()

    assert response.status_code == 200
    assert actual == expected


def test_get_package_details(client, mock_parse_status_file):
    """
    Tests the normal case for getting package details
    """
    response = client.get('api/packages/tcpd')
    assert response.status_code == 200

    expected = {
        'name': 'tcpd',
        'description': r"Wietse Venema's TCP wrapper utilities\n Wietse Venema's network logger, also known as TCPD or LOG_TCP.",
        'dependencies': [
            [
                {'name': 'libc6', 'version': '>= 2.4', 'url': None}
            ],
            [
                {'name': 'libwrap0', 'version': '>= 7.6-4~', 'url': '/packages/libwrap0'}
            ]
        ],
        'reverse_dependencies': []
    }

    actual = response.get_json()

    assert actual == expected


def test_get_package_details_nonexistent(client, mock_parse_status_file):
    """
    Tests the case where a package does not exist and ensures a 404 error is returned.
    """
    response = client.get('api/packages/nonexistent')
    assert response.status_code == 404

    json_data = response.get_json()
    assert json_data['error'] == 'Not Found'
    assert json_data['message'] == 'Package not found'


def test_parse_dependencies(packages):
    """
    Test the normal case for parsing dependencies
    """
    dependencies = ['libc6 (>= 2.4) | libc6.1 (>= 2.4)', 'libwrap0 (>= 7.6-4~)']
    expected = [
        [
            {'name': 'libc6', 'version': '>= 2.4', 'url': None},
            {'name': 'libc6.1', 'version': '>= 2.4', 'url': None}
        ],
        [
            {'name': 'libwrap0', 'version': '>= 7.6-4~', 'url': '/packages/libwrap0'}
        ]
    ]

    actual = parse_dependencies(dependencies, packages)

    assert actual == expected


def test_parse_dependencies_no_deps():
    """
    Tests the case where there are no dependencies and ensures an empty list is returned.
    """
    dependencies = []
    packages = {}
    expected = []
    actual = parse_dependencies(dependencies, packages)
    assert actual == expected


def test_get_reverse_dependencies(packages):
    """
    Tests the normal case for getting reverse dependencies
    """
    expected = ['tcpd']
    actual = get_reverse_dependencies('libwrap0', packages)
    assert actual == expected


def test_get_reverse_dependencies_no_reverse_deps(packages):
    """
    Tests the case where there are no reverse dependencies and ensures an empty list is returned.
    """
    expected = []
    actual = get_reverse_dependencies('nonexistent', packages)

    assert actual == expected


def test_get_all_packages_parse_error(mocker):
    """
    Tests the case where `parse_status_file()` raises an exception and ensures the exception is properly raised.
    """
    mocker.patch('app.services.parse_status_file', side_effect=Exception('Parse error'))
    with pytest.raises(Exception) as excinfo:
        get_all_packages()

    assert 'Parse error' in str(excinfo.value)

