from flask import Flask

import logging
from app.config import DevelopmentConfig
from app.routes.api import api_bp
from app.routes.frontend import frontend_bp
from app.errors import errors as errors_bp


def create_app(config_class=DevelopmentConfig):
    """
    Creates the Flask app and utilizes the routes & errors blueprints.
    :return: Returns the Flask application
    """
    app = Flask(__name__)
    app.config.from_object(config_class)
    app.register_blueprint(api_bp)
    app.register_blueprint(frontend_bp)
    app.register_blueprint(errors_bp)

    logging.basicConfig(
        level=app.config['LOG_LEVEL'],
        handlers=[
            logging.StreamHandler(),
            logging.FileHandler(app.config['LOG_FILE'])
        ]
    )
    app.logger.setLevel(app.config['LOG_LEVEL'])

    return app
