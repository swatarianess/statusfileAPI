import re

from app.util.parser import parse_status_file


def strip_version_info(dependency):
    """
    Removes version info from the dependency string

    Example: 'libc6 (>= 2.4)' -> 'libc6'

    :param dependency: The string containing the package
    :return: Returns the dependency name
    """
    return re.split(r' \(.*\)', dependency)[0].strip()


def parse_dependency(dependency):
    """
    Parse a dependency string to separate the name and version information.
    :param dependency: The dependency to parse
    :return: Returns a dict with the name and version of a dependency
    """
    match = re.match(r'([^ ]+) \(([^)]+)\)', dependency)
    if match:
        return {'name': match.group(1), 'version': match.group(2)}
    return {'name': dependency.strip(), 'version': ''}


def parse_dependencies(dependencies, packages):
    """
    Parse the dependencies of a package

    :param dependencies: The dependencies to parse
    :param packages: The pa
    :return: Returns all the dependencies

    """
    parsed_dep = []

    for dep in dependencies:
        alternatives = dep.split(' | ')
        parsed_alternatives = []
        for alt in alternatives:
            parsed_alt = parse_dependency(alt)
            alt_name = parsed_alt['name']
            alt_ver = parsed_alt['version']

            parsed_alternatives.append({
                'name': alt_name,
                'version': alt_ver,
                'url': f'/packages/{alt_name}' if alt_name in packages else None
            })

        parsed_dep.append(parsed_alternatives)
    return parsed_dep


def get_all_packages():
    """
    Returns all packages
    :return: Returns a list of packages
    """
    packages = parse_status_file()
    return [{"name": pkg, "version": packages[pkg].get('Version', '')} for pkg in packages]


def get_reverse_dependencies(package_name, packages):
    """
    Generates the reverse dependencies
    :param package_name: The given package name to find reversed dependencies
    :param packages: A list of packages
    :return: Returns the reversed dependencies
    """
    reverse_deps = []
    for pkg, details in packages.items():
        if 'Depends' in details:
            dependencies = details['Depends'].split(', ')
            for dep in dependencies:
                alternatives = dep.split(' | ')
                for alt in alternatives:
                    alt_name = strip_version_info(alt)
                    if package_name == alt_name:
                        reverse_deps.append(pkg)
                        break
    return reverse_deps


def get_package_details(package_name):
    """
    Retrieve package details given a package name
    :param package_name: The name of the package to retrieve details information
    :return: Returns the package details
    """
    packages = parse_status_file()
    if package_name not in packages:
        return None

    package = packages[package_name]
    dependencies = package.get('Depends', '').split(', ') if 'Depends' in package else []
    parsed_dependencies = parse_dependencies(dependencies, packages)
    reverse_dependencies = get_reverse_dependencies(package_name, packages)

    return {
        'name': package_name,
        'description': package.get('Description', ''),
        'dependencies': parsed_dependencies,
        'reverse_dependencies': [
            {
                'name': rev_dep,
                'url': f'/packages/{rev_dep}'
            } for rev_dep in reverse_dependencies
        ]
    }
