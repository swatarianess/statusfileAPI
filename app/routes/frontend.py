from flask import Blueprint, render_template, current_app
from app.services import get_all_packages, get_package_details

frontend_bp = Blueprint('frontend', __name__)

@frontend_bp.route('/')
def index():
    try:
        packages = get_all_packages()
        return render_template('index.html', packages=packages)
    except Exception as e:
        current_app.logger.error(f"Error getting packages: {str(e)}")
        return render_template('index.html', packages=[])


@frontend_bp.route('/packages/<string:package_name>')
def package_details(package_name):
    try:
        package = get_package_details(package_name)
        if not package:
            current_app.logger.warning(f"Package not found: {package_name}")
            return render_template('package_details.html', package=None)
        return render_template('package_details.html', package=package)
    except Exception as e:
        current_app.logger.error(f"error getting package details for {package_name}: {str(e)}")
        return render_template('package_details.html', package=None)

