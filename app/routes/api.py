from flask import Blueprint, jsonify, current_app
from app.services import get_all_packages, get_package_details

api_bp = Blueprint('api', __name__, url_prefix='/api')


@api_bp.route('/packages', methods=['GET'])
def get_pkgs():
    try:
        packages = get_all_packages()
        return jsonify(packages)
    except Exception as e:
        current_app.logger.error(f"Error getting packages: {str(e)}")
        return jsonify({"error": "Internal Server Error", "message": str(e)}), 500


@api_bp.route('/packages/<string:package_name>', methods=['GET'])
def get_pkg_details(package_name):
    try:
        package = get_package_details(package_name)
        if not package:
            return jsonify({"error": "Not Found", "message": "Package not found"}), 404
        return jsonify(package)
    except Exception as e:
        current_app.logger.error(f"Error getting package details for {package_name}: {str(e)}")
        return jsonify({"error": "Internal Server Error", "message": str(e)}), 500
