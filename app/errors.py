from flask import jsonify, Blueprint

errors = Blueprint('errors', __name__)


@errors.errorhandler(404)
def handle_404_error(_error):
    return jsonify({'error': 'Not found', "message": "Resource not found"}), 404


@errors.errorhandler(500)
def handle_500_error(_error):
    return jsonify({'error': 'Internal server error', "message": "An unexpected error occured"}), 500


@errors.app_errorhandler(400)
def handle_400_error(_error):
    return jsonify({"error": "Bad Request", "message": "The request could not be understood by the server"}), 400


@errors.app_errorhandler(403)
def handle_403_error(_error):
    return jsonify({"error": "Forbidden", "message": "You don't have the permission to access the requested resource"}), 403
