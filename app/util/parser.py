import os


def parse_status_file(status_file_path=None):
    """
    Parses the status file from the Filesystem
    :return: Returns a dict with all installed packages
    """
    if status_file_path is None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        status_file_path = os.path.abspath(os.path.join(current_dir, '../data/status'))

    packages = {}
    with open(status_file_path, 'r', encoding='utf-8') as file:
        package = {}
        for line in file:
            if line.strip() == '':
                if 'Package' in package:
                    packages[package['Package']] = package
                package = {}
            elif ': ' in line:
                key, value = line.split(': ', 1)
                package[key.strip()] = value.strip()
            else:
                continue
        if 'Package' in package:
            packages[package['Package']] = package
    return packages
