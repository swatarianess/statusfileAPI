import os

BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

class Config:
    DEBUG = True
    LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')
    LOG_FILE = os.path.join(BASE_DIR, 'app.log')


class DevelopmentConfig(Config):
    DEBUG = True
    LOG_LEVEL = os.getenv('LOG_LEVEL', 'DEBUG')


class TestingConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')
    DEBUG = False
    TESTING = False
