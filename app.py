import os

from app import create_app

config = os.getenv('FLASK_CONFIG', 'DevelopmentConfig')
app = create_app(config_class=getattr(__import__('app.config', fromlist=[config]), config))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
